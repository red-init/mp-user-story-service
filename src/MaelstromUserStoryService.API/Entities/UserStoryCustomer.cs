﻿namespace MaelstromUserStoryService.API.Entities
{
    public class UserStoryCustomer
    {
        public int? UserStoryId { get; set; }
        public UserStory? UserStory { get; set; }

        public int? CustomerId { get; set; }
        //public Customer? Customer { get; set; }
    }
}