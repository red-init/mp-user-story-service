﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MaelstromUserStoryService.API.Entities
{
    public class UserStoryTheme
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? UserStoryThemeId { get; set; }

        [Required]
        [MaxLength(72)]
        public string? Name { get; set; }

        [MaxLength(128)]
        public string? Description { get; set; }

        [MaxLength(128)]
        public string? Usage { get; set; }

        public List<UserStory>? UserStories { get; set; }
    }
}