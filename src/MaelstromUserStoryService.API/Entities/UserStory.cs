﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MaelstromUserStoryService.API.Entities
{
    public class UserStory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? UserStoryId { get; set; }

        [Required]
        [MaxLength(72)]
        public string? Title { get; set; }

        public int? Owner { get; set; }
        public int? Author { get; set; }
        public int? Champions { get; set; }
        public int? Teams { get; set; }
        public int? Customers { get; set; }
        public int? Products { get; set; }
        public int? Projects { get; set; }
        public DateTime? OriginDate { get; set; }
        public int? State { get; set; }
        public int? Sequence { get; set; }

        [MaxLength(512)]
        public string? Content { get; set; }

        public int? UserStoryThemeId { get; set; }
    }
}
