﻿using System.ComponentModel.DataAnnotations;

namespace MaelstromUserStoryService.API.Entities
{
    public class UserStoryChampion
    {
        [Key]
        public int? UserStoryChampionId { get; set; }
        public int? UserStoryId { get; set; }
        public int? PersonId { get; set; }
    }
}
