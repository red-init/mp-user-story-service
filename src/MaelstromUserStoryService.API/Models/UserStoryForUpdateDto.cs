﻿namespace MaelstromUserStoryService.API.Models
{
    public class UserStoryForUpdateDto
    {
        public string? Title { get; set; }
        public int? Owner { get; set; }
        public int? Author { get; set; }
        public int? Champions { get; set; }
        public int? Teams { get; set; }
        public int? Customers { get; set; }
        public int? Products { get; set; }
        public int? Projects { get; set; }
        public DateTime? OriginDate { get; set; }
        public int? State { get; set; }
        public int? Sequence { get; set; }
        public string? Content { get; set; }
        public int? UserStoryThemeId { get; set; }
    }
}