﻿using MaelstromUserStoryService.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace MaelstromUserStoryService.API.Contexts
{
    public class UserStoryContext : DbContext
    {
        public virtual DbSet<UserStoryTheme> UserStoryThemes { get; set; }
        public virtual DbSet<UserStoryChampion> UserStoryChampions { get; set; }
        public virtual DbSet<UserStory> UserStories { get; set; }

        public UserStoryContext(){}

        public UserStoryContext(DbContextOptions<UserStoryContext> options)
           : base(options)
        {
            base.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            UserStoryTheme featureTheme = new()
            {
                UserStoryThemeId = 1,
                Name = "Feature",
                Description = "This is a user story for a feature.",
                Usage = "Use this theme for a new feature."
            };

            UserStoryTheme enhancementTheme = new()
            {
                UserStoryThemeId = 2,
                Name = "Enhancement",
                Description = "This is a user story for an enhancement to an existing feature.",
                Usage = "Use this theme for an enhancement to an existing feature."
            };

            modelBuilder.Entity<UserStoryTheme>()
                .HasData(
                    featureTheme,
                    enhancementTheme
                );

            modelBuilder.Entity<UserStory>()
                 .HasData(
                new UserStory()
                {
                    UserStoryId = 1,
                    UserStoryThemeId = 1,
                    Title = "New York City",
                    Content = "The one with that big park.",
                },
                new UserStory()
                {
                    UserStoryId = 2,
                    UserStoryThemeId = 2,
                    Title = "Antwerp",
                    Content = "The one with the cathedral that was never really finished.",
                },
                new UserStory()
                {
                    UserStoryId = 3,
                    UserStoryThemeId = 1,
                    Title = "Paris",
                    Content = "The one with that big tower.",
                });

            base.OnModelCreating(modelBuilder);
        }
    }
}
