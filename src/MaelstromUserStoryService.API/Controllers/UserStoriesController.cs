﻿using AutoMapper;
using MaelstromUserStoryService.API.Contexts;
using MaelstromUserStoryService.API.Entities;
using MaelstromUserStoryService.API.Helpers;
using MaelstromUserStoryService.API.Models;
using MaelstromUserStoryService.API.Services;

using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Serilog;


namespace MaelstromUserStoryService.API.Controllers
{
    [ApiController]
    [Route("api/userstories")]
    public class UserStoriesController : ControllerBase
    {
        private readonly IUserStoryRepository _userStoryRepository;
        private readonly IMapper _mapper;

        public UserStoriesController(IUserStoryRepository userStoryRepository,
            IMapper mapper)
        {
            _userStoryRepository = userStoryRepository ??
                                  throw new ArgumentNullException(nameof(userStoryRepository));
            _mapper = mapper ??
                throw new ArgumentNullException(nameof(mapper));
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetUserStories()
        {
            var userStoryEntities = _userStoryRepository.GetUserStories();
            Log.Information("Testing... log...");

            return Ok(_mapper.Map<IEnumerable<UserStoryWithoutRequirementsDto>>(userStoryEntities));
        }

        [Authorize]
        [HttpGet("{id}", Name = "GetUserStory")]
        public IActionResult GetUserStory(int id, bool includeRequirements = false)
        {
            var city = _userStoryRepository.GetUserStory(id, includeRequirements);

            if (city == null)
            {
                return NotFound();
            }

            if (includeRequirements)
            {
                return Ok(_mapper.Map<UserStoryDto>(city));
            }

            return Ok(_mapper.Map<UserStoryWithoutRequirementsDto>(city));
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateUserStory(
            [FromBody] UserStoryForCreationDto userStory)
        {
            if (userStory.Content == userStory.Title)
            {
                ModelState.AddModelError(
                    "Description",
                    "The provided description should be different from the name.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //if (!_userStoryRepository.UserStoryExists(userStoryId))
            //{
            //    return NotFound();
            //}

            var finalUserStory = _mapper.Map<Entities.UserStory>(userStory);

            _userStoryRepository.AddUserStory(finalUserStory);

            _userStoryRepository.Save();

            var createdUserStoryToReturn = _mapper
                .Map<Models.UserStoryDto>(finalUserStory);

            foreach (var champion in userStory.Champions)
            {
                _userStoryRepository.AddChampion(createdUserStoryToReturn.UserStoryId, champion);
            }

            return CreatedAtRoute(
                "GetUserStory",
                new { id = createdUserStoryToReturn.UserStoryId },
                createdUserStoryToReturn);


        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult DeleteUserStory(int id)
        {
            if (!_userStoryRepository.UserStoryExists(id))
            {
                return NotFound();
            }

            var userStoryEntity = _userStoryRepository
                .GetUserStory(id, false);
            if (userStoryEntity == null)
            {
                return NotFound();
            }

            _userStoryRepository.DeleteUserStory(userStoryEntity);

            _userStoryRepository.Save();

            return NoContent();
        }

        [Authorize]
        [HttpPut("{id}")]
        public IActionResult UpdateUserStory(int id,
            [FromBody] UserStoryForUpdateDto userStory)
        {
            if (userStory.Content == userStory.Title)
            {
                ModelState.AddModelError(
                    "Description",
                    "The provided description should be different from the name.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_userStoryRepository.UserStoryExists(id))
            {
                return NotFound();
            }
            else
            {
                UserStory? userStoryEntity = _userStoryRepository.GetUserStory(id, false);
                _mapper.Map(userStory, userStoryEntity);
                _userStoryRepository.UpdateUserStory(userStoryEntity);
                _userStoryRepository.Save();

                return NoContent();
            }
        }

        [Authorize]
        [HttpPatch("{id}")]
        public IActionResult PartiallyUpdateUserStory(int id,
            [FromBody] JsonPatchDocument<UserStoryForUpdateDto> patchDoc)
        {
            if (!_userStoryRepository.UserStoryExists(id))
            {
                return NotFound();
            }

            var userStoryEntity = _userStoryRepository
                .GetUserStory(id, false);
            if (userStoryEntity == null)
            {
                return NotFound();
            }

            var userStoryToPatch = _mapper
                .Map<UserStoryForUpdateDto>(userStoryEntity);

            patchDoc.ApplyTo(userStoryToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userStoryToPatch.Content == userStoryToPatch.Title)
            {
                ModelState.AddModelError(
                    "Description",
                    "The provided description should be different from the name.");
            }

            if (!TryValidateModel(userStoryToPatch))
            {
                return BadRequest(ModelState);
            }

            _mapper.Map(userStoryToPatch, userStoryEntity);

            _userStoryRepository.UpdateUserStory(userStoryEntity);

            _userStoryRepository.Save();

            return NoContent();
        }
    }
}
