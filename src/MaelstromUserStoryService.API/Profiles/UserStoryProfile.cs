﻿using AutoMapper;

namespace MaelstromUserStoryService.API.Profiles
{
    public class UserStoryProfile : Profile
    {
        public UserStoryProfile()
        {
            CreateMap<Entities.UserStory, Models.UserStoryWithoutRequirementsDto>();
            CreateMap<Entities.UserStory, Models.UserStoryDto>();

            CreateMap<Models.UserStoryForCreationDto, Entities.UserStory>();
            CreateMap<Models.UserStoryForUpdateDto, Entities.UserStory>().ReverseMap();
        }
    }
}
