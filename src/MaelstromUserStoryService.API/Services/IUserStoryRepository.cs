﻿using MaelstromUserStoryService.API.Entities;

namespace MaelstromUserStoryService.API.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUserStoryRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<UserStory> GetUserStories();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userStoryId"></param>
        /// <param name="includeRequirements"></param>
        /// <returns></returns>
        UserStory? GetUserStory(int userStoryId, bool includeRequirements);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userStoryId"></param>
        /// <returns></returns>
        bool UserStoryExists(int userStoryId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userStory"></param>
        void AddUserStory(UserStory? userStory);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userStory"></param>
        void UpdateUserStory(UserStory? userStory);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userStory"></param>
        void DeleteUserStory(UserStory userStory);

        void AddChampion(int? userStoryId, int? championId);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool Save();
    }
}
