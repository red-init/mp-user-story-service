﻿using MaelstromUserStoryService.API.Contexts;
using MaelstromUserStoryService.API.Entities;

namespace MaelstromUserStoryService.API.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class UserStoryRepository : IUserStoryRepository
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly UserStoryContext _context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public UserStoryRepository(UserStoryContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public IEnumerable<UserStory> GetUserStories()
        {
            if (_context.UserStories != null)
            {
                return _context.UserStories.OrderBy(c => c.Title).ToList();
            }
            else
            {
                throw new ArgumentNullException(nameof(_context.UserStories));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userStoryId"></param>
        /// <param name="includeRequirements"></param>
        /// <returns></returns>
        public UserStory? GetUserStory(int userStoryId, bool includeRequirements)
        {
            UserStory? us = null;

            if (_context.UserStories != null)
            {
                us = _context.UserStories.FirstOrDefault(c => c.UserStoryId == userStoryId);
            }

            return us;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userStoryId"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public bool UserStoryExists(int userStoryId)
        {
            if (_context.UserStories != null)
            {
                return _context.UserStories.Any(c => c.UserStoryId == userStoryId);
            }
            else
            {
                throw new ArgumentNullException(nameof(_context.UserStories));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userStory"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public void AddUserStory(UserStory? userStory)
        {
            if (userStory != null)
            {
                if (_context.UserStories != null)
                {
                    _context.UserStories.Add(userStory);
                }
                else
                {
                    throw new ArgumentNullException(nameof(userStory));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userStory"></param>
        public void UpdateUserStory(UserStory? userStory)
        {
            if (userStory != null)
            {
                _context.UserStories?.Update(userStory);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userStory"></param>
        public void DeleteUserStory(UserStory userStory)
        {
            _context.UserStories.Remove(userStory);
        }

        /// <summary>
        /// Adds the champion.
        /// </summary>
        /// <param name="userStoryId">The user story identifier.</param>
        /// <param name="championId">The champion identifier.</param>
        public void AddChampion(int? userStoryId, int? championId)
        {
            var champion = new UserStoryChampion();
            champion.UserStoryId = userStoryId;
            champion.PersonId = championId;

            _context.UserStoryChampions.Add(champion);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}
