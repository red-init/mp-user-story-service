﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MaelstromUserStoryService.API.Contexts;
using MaelstromUserStoryService.API.Controllers;
using MaelstromUserStoryService.API.Entities;
using MaelstromUserStoryService.API.Profiles;
using MaelstromUserStoryService.API.Services;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Moq;

namespace MaelstromUserStoryService.Tests
{
    public class MockedControllerTestSuite
    {
        private readonly Mock<IUserStoryRepository> _mockRepo;
        private readonly UserStoriesController _controller;

        public MockedControllerTestSuite()
        {
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new UserStoryProfile()); });
            var mapper = mappingConfig.CreateMapper();

            _mockRepo = new Mock<IUserStoryRepository>();
            _controller = new UserStoriesController(_mockRepo.Object, mapper);
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok1()
        {
            // Arrange



            // Act
            IActionResult result = _controller.GetUserStories();
            var items = result as OkObjectResult;
            
            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok2()
        {
            // Arrange



            // Act
            IActionResult result = _controller.GetUserStories();
            var items = result as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok3()
        {
            // Arrange



            // Act
            IActionResult result = _controller.GetUserStories();
            var items = result as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok4()
        {
            // Arrange



            // Act
            IActionResult result = _controller.GetUserStories();
            var items = result as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok5()
        {
            // Arrange



            // Act
            IActionResult result = _controller.GetUserStories();
            var items = result as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok6()
        {
            // Arrange



            // Act
            IActionResult result = _controller.GetUserStories();
            var items = result as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok7()
        {
            // Arrange



            // Act
            IActionResult result = _controller.GetUserStories();
            var items = result as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok8()
        {
            // Arrange



            // Act
            IActionResult result = _controller.GetUserStories();
            var items = result as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok9()
        {
            // Arrange



            // Act
            IActionResult result = _controller.GetUserStories();
            var items = result as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok10()
        {
            // Arrange



            // Act
            IActionResult result = _controller.GetUserStories();
            var items = result as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
        }
    }
}
