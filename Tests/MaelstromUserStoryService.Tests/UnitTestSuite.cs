﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using AutoMapper;
using MaelstromUserStoryService.API.Contexts;
using MaelstromUserStoryService.API.Controllers;
using MaelstromUserStoryService.API.Entities;
using MaelstromUserStoryService.API.Profiles;
using MaelstromUserStoryService.API.Services;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Xunit;
using Moq;

namespace MaelstromUserStoryService.Tests
{
    public class RepositoryFixture : IDisposable
    {
        public RepositoryFixture()
        {
            var options = new DbContextOptionsBuilder<UserStoryContext>();
            options.UseInMemoryDatabase("UnitTestDB");

            var context = new UserStoryContext(options.Options);

            Repository = new UserStoryRepository(context);
        }

        public UserStoryRepository Repository { get; set; }

        public void Dispose()
        {

        }
    }

    //public class RepositoryFixture : IDisposable
    //{
    //    public RepositoryFixture()
    //    {
    //        var moviesMock = CreateDbSetMock(GetFakeListOfMovies());
    //        var mockDbContext = new Mock<UserStoryContext>();
    //        mockDbContext.Setup(x => x.UserStories).Returns(moviesMock.Object);
    //        Repository = new UserStoryRepository(mockDbContext.Object);
    //    }

    //    public UserStoryRepository Repository { get; set; }

    //    private IEnumerable<UserStory> GetFakeListOfMovies()
    //    {
    //        var movies = new List<UserStory>
    //        {
    //            new UserStory {UserStoryId = 1, Title = "Movie 1"},
    //            new UserStory {UserStoryId = 2, Title = "Movie 2"},
    //            new UserStory {UserStoryId = 3, Title = "Movie 3"}
    //        };

    //        return movies;
    //    }

    //    private static Mock<DbSet<T>> CreateDbSetMock<T>(IEnumerable<T> elements) where T : class
    //    {
    //        var elementsAsQueryable = elements.AsQueryable();
    //        var dbSetMock = new Mock<DbSet<T>>();

    //        dbSetMock.As<IQueryable<T>>().Setup(m => m.Provider).Returns(elementsAsQueryable.Provider);
    //        dbSetMock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(elementsAsQueryable.Expression);
    //        dbSetMock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(elementsAsQueryable.ElementType);
    //        dbSetMock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(elementsAsQueryable.GetEnumerator());

    //        return dbSetMock;
    //    }
    //    public void Dispose()
    //    {

    //    }
    //}

    public class AllFixture : IDisposable
    {
        public AllFixture()
        {
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new UserStoryProfile()); });
            var mapper = mappingConfig.CreateMapper();

            List<UserStory> userStories = new()
            {
                new UserStory
                {
                    UserStoryId = 1,
                    Title = "One",
                    Content = "User story one."
                },
                new UserStory
                {
                    UserStoryId = 2,
                    Title = "Two",
                    Content = "User story two."
                }
            };

            Repository = new Mock<IUserStoryRepository>();
            Repository.Setup(x => x.GetUserStories()).Returns(userStories);
            Controller = new UserStoriesController(Repository.Object, mapper);
        }

        public Mock<IUserStoryRepository> Repository { get; set; }
        public UserStoriesController Controller { get; set; }

        public void Dispose()
        {

        }
    };

    [CollectionDefinition("Controller Collection")]
    public class ControllerCollection : ICollectionFixture<AllFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }

    [CollectionDefinition("Repository Collection")]
    public class RepositoryCollection : ICollectionFixture<RepositoryFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }

    [Collection("Controller Collection")]
    public class ControllerTestSuite
    {
        private readonly AllFixture _allFixture;

        public ControllerTestSuite(AllFixture allFixture)
        {
            this._allFixture = allFixture;
        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok1()
        {


            // Arrange
            //IUserStoryRepository rpo = _allFixture.Repository.Object;
            UserStoriesController ctr = _allFixture.Controller;

            // Act
            IActionResult result = ctr.GetUserStories();
            var items = result as OkObjectResult;

            var i = items.Value as List<UserStory>;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);
            

        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok2()
        {


            // Arrange
            //IUserStoryRepository rpo = _allFixture.Repository.Object;
            UserStoriesController ctr = _allFixture.Controller;

            // Act
            IActionResult result = ctr.GetUserStories();
            var items = result as OkObjectResult;

            var i = items.Value as List<UserStory>;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);

        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok3()
        {


            // Arrange
            //IUserStoryRepository rpo = _allFixture.Repository.Object;
            UserStoriesController ctr = _allFixture.Controller;

            // Act
            IActionResult result = ctr.GetUserStories();
            var items = result as OkObjectResult;

            var i = items.Value as List<UserStory>;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);

        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok4()
        {


            // Arrange
            //IUserStoryRepository rpo = _allFixture.Repository.Object;
            UserStoriesController ctr = _allFixture.Controller;

            // Act
            IActionResult result = ctr.GetUserStories();
            var items = result as OkObjectResult;

            var i = items.Value as List<UserStory>;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);

        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok5()
        {


            // Arrange
            //IUserStoryRepository rpo = _allFixture.Repository.Object;
            UserStoriesController ctr = _allFixture.Controller;

            // Act
            IActionResult result = ctr.GetUserStories();
            var items = result as OkObjectResult;

            var i = items.Value as List<UserStory>;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);

        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok6()
        {


            // Arrange
            //IUserStoryRepository rpo = _allFixture.Repository.Object;
            UserStoriesController ctr = _allFixture.Controller;

            // Act
            IActionResult result = ctr.GetUserStories();
            var items = result as OkObjectResult;

            var i = items.Value as List<UserStory>;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);

        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok7()
        {


            // Arrange
            //IUserStoryRepository rpo = _allFixture.Repository.Object;
            UserStoriesController ctr = _allFixture.Controller;

            // Act
            IActionResult result = ctr.GetUserStories();
            var items = result as OkObjectResult;

            var i = items.Value as List<UserStory>;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);

        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok8()
        {


            // Arrange
            //IUserStoryRepository rpo = _allFixture.Repository.Object;
            UserStoriesController ctr = _allFixture.Controller;

            // Act
            IActionResult result = ctr.GetUserStories();
            var items = result as OkObjectResult;

            var i = items.Value as List<UserStory>;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);

        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok9()
        {


            // Arrange
            //IUserStoryRepository rpo = _allFixture.Repository.Object;
            UserStoriesController ctr = _allFixture.Controller;

            // Act
            IActionResult result = ctr.GetUserStories();
            var items = result as OkObjectResult;

            var i = items.Value as List<UserStory>;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);

        }

        [Fact]
        public void Controller_GetUserStories_Returns_Ok10()
        {


            // Arrange
            //IUserStoryRepository rpo = _allFixture.Repository.Object;
            UserStoriesController ctr = _allFixture.Controller;

            // Act
            IActionResult result = ctr.GetUserStories();
            var items = result as OkObjectResult;

            var i = items.Value as List<UserStory>;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(items);

        }


        //[Fact]
        //public void Controller_GetUserStories_Returns_Ok2()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;
        //    UserStoriesController ctr = _allFixture.Controller;

        //    // Act
        //    IActionResult result = ctr.GetUserStory(1);
        //    var items = result as OkObjectResult;

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(items);
        //}

        //[Fact]
        //public void Controller_GetUserStories_Returns_Ok3()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;
        //    UserStoriesController ctr = _allFixture.Controller;

        //    // Act
        //    IActionResult result = ctr.GetUserStory(2);
        //    var items = result as OkObjectResult;

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(items);
        //}

        //[Fact]
        //public void Controller_GetUserStories_Returns_Ok4()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;
        //    UserStoriesController ctr = _allFixture.Controller;

        //    // Act
        //    IActionResult result = ctr.GetUserStory(3);
        //    var items = result as OkObjectResult;

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(items);
        //}

        //[Fact]
        //public void Controller_GetUserStories_Returns_Ok5()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;
        //    UserStoriesController ctr = _allFixture.Controller;

        //    // Act
        //    IActionResult result = ctr.GetUserStory(4);
        //    var items = result as OkObjectResult;

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(items);
        //}

        //[Fact]
        //public void Controller_GetUserStories_Returns_Ok6()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;
        //    UserStoriesController ctr = _allFixture.Controller;

        //    // Act
        //    IActionResult result = ctr.GetUserStory(5);
        //    var items = result as OkObjectResult;

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(items);
        //}

        //[Fact]
        //public void Controller_GetUserStories_Returns_Ok7()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;
        //    UserStoriesController ctr = _allFixture.Controller;

        //    // Act
        //    IActionResult result = ctr.GetUserStory(6);
        //    var items = result as OkObjectResult;

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(items);
        //}

        //[Fact]
        //public void Controller_GetUserStories_Returns_Ok8()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;
        //    UserStoriesController ctr = _allFixture.Controller;

        //    // Act
        //    IActionResult result = ctr.GetUserStory(8);
        //    var items = result as OkObjectResult;

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(items);
        //}

        //[Fact]
        //public void Controller_GetUserStories_Returns_Ok9()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;
        //    UserStoriesController ctr = _allFixture.Controller;

        //    // Act
        //    IActionResult result = ctr.GetUserStory(9);
        //    var items = result as OkObjectResult;

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(items);
        //}

        //[Fact]
        //public void Controller_GetUserStories_Returns_Ok10()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;
        //    UserStoriesController ctr = _allFixture.Controller;

        //    // Act
        //    IActionResult result = ctr.GetUserStory(10);
        //    var items = result as OkObjectResult;

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(items);
        //}
    }

    [Collection("Repository Collection")]
    public class RepositoryTestSuite
    {
        private readonly RepositoryFixture _allFixture;

        public RepositoryTestSuite(RepositoryFixture allFixture)
        {
            this._allFixture = allFixture;
        }

        [Fact]
        public void Repository_GetUserStories_Returns_None1()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            IEnumerable<UserStory> items = rpo.GetUserStories();

            // Assert
            //Assert.Empty(items);

        }

        [Fact]
        public void Repository_GetUserStories_Returns_None11()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            IEnumerable<UserStory> items = rpo.GetUserStories();

            // Assert
            //Assert.Empty(items);

        }

        [Fact]
        public void Repository_GetUserStories_Returns_None2()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            UserStory items = rpo.GetUserStory(1, false);

            // Assert
            //Assert.Empty(items);

        }

        [Fact]
        public void Repository_GetUserStories_Returns_None3()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            UserStory items = rpo.GetUserStory(2, false);

            // Assert
            //Assert.Empty(items);

        }

        [Fact]
        public void Repository_GetUserStories_Returns_None4()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            UserStory items = rpo.GetUserStory(3, false);

            // Assert
            //Assert.Empty(items);

        }

        [Fact]
        public void Repository_GetUserStories_Returns_None5()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            UserStory items = rpo.GetUserStory(4, false);

            // Assert
            //Assert.Empty(items);

        }

        [Fact]
        public void Repository_GetUserStories_Returns_None6()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            UserStory items = rpo.GetUserStory(5, false);

            // Assert
            //Assert.Empty(items);

        }

        [Fact]
        public void Repository_GetUserStories_Returns_None7()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            UserStory items = rpo.GetUserStory(6, false);

            // Assert
            //Assert.Empty(items);

        }

        [Fact]
        public void Repository_GetUserStories_Returns_None8()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            UserStory items = rpo.GetUserStory(7, false);

            // Assert
            //Assert.Empty(items);

        }

        [Fact]
        public void Repository_GetUserStories_Returns_None9()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            UserStory items = rpo.GetUserStory(8, false);

            // Assert
            //Assert.Empty(items);

        }

        [Fact]
        public void Repository_GetUserStories_Returns_None10()
        {
            // Arrange
            UserStoryRepository rpo = _allFixture.Repository;

            // Act
            UserStory items = rpo.GetUserStory(9, false);

            // Assert
            //Assert.Empty(items);

        }


        //[Fact]
        //public void Repository_GetUserStories_Returns_Three()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;

        //    // Act
        //    var item = rpo.GetUserStory(1, false);

        //    // Assert
        //    Assert.Equal(1, 1);

        //}

        //[Fact]
        //public void Repository_GetUserStory_Returns_None()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;

        //    // Act
        //    var item = rpo.GetUserStory(2, false);

        //    // Assert
        //    Assert.Equal(1, 1);

        //}

        //[Fact]
        //public void Repository_GetUserStory_Returns_One()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;

        //    // Act
        //    var item = rpo.GetUserStory(3, false);

        //    // Assert
        //    Assert.Equal(1, 1);

        //}

        //[Fact]
        //public void Repository_UserStoryExists_Returns_False()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;

        //    // Act
        //    var item = rpo.GetUserStory(4, false);

        //    // Assert
        //    Assert.Equal(1, 1);

        //}

        //[Fact]
        //public void Repository_UserStoryExists_Returns_True()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;

        //    // Act
        //    var item = rpo.GetUserStory(5, false);

        //    // Assert
        //    Assert.Equal(1, 1);

        //}

        //[Fact]
        //public void Repository_AddUserStory_Returns_New()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;

        //    // Act
        //    var item = rpo.GetUserStory(6, false);

        //    // Assert
        //    Assert.Equal(1, 1);

        //}

        //[Fact]
        //public void Repository_UpdateUserStory_Returns_One()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;

        //    // Act
        //    var item = rpo.GetUserStory(7, false);

        //    // Assert
        //    Assert.Equal(1, 1);

        //}

        //[Fact]
        //public void Repository_UpdateUserStory_Returns_Null()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;

        //    // Act
        //    var item = rpo.GetUserStory(8, false);

        //    // Assert
        //    Assert.Equal(1, 1);

        //}

        //[Fact]
        //public void Repository_DeleteUserStory_Returns_One()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;

        //    // Act
        //    var item = rpo.GetUserStory(9, false);

        //    // Assert
        //    Assert.Equal(1, 1);

        //}

        //[Fact]
        //public void Repository_DeleteUserStory_Returns_Null()
        //{
        //    // Arrange
        //    IUserStoryRepository rpo = _allFixture.Repository.Object;

        //    // Act
        //    var item = rpo.GetUserStory(10, false);

        //    // Assert
        //    Assert.Equal(1, 1);

        //}
    }
}
