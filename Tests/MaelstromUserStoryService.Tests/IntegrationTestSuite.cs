using System;
using MaelstromUserStoryService.API.Models;

using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Hosting;

using Newtonsoft.Json;

using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace MaelstromUserStoryService.Tests
{
    public class AlphabeticalOrderer : ITestCaseOrderer
    {
        public IEnumerable<TTestCase> OrderTestCases<TTestCase>(
            IEnumerable<TTestCase> testCases) where TTestCase : ITestCase
        {
            return testCases.OrderBy(testCase => testCase.TestMethod.Method.Name);
        }
    }

    [TestCaseOrderer("MaelstromUserStoryService.Tests.AlphabeticalOrderer", "MaelstromUserStoryService.Tests")]
    public class ApiTests
    {
        [Fact]
        public async Task T1_GetUserStories_Returns_OK()
        {
            await using UserStoryApi? application = new();

            var client = application.CreateClient();
            var response = await client.GetAsync("/api/userstories");

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task T2_CreateUserStory()
        {
            DatabaseOps.Truncate();

            await using UserStoryApi? application = new();

            var client = application.CreateClient();

            // Arrange
            var newItem = new UserStoryDto
            {
                Title = "Create a User Story",
                Owner = 1,
                Author = 1,
                Champions = 1,
                Teams = 1,
                Customers = 1,
                Products = 1,
                Projects = 1,
                OriginDate = DateTime.Parse("2021-12-28"),
                State = 1,
                Sequence = 1,
                Content = "As a <>, I want to <>, so that I can <>.",
                UserStoryThemeId = 1,
            };

            // Act - Add a new item
            using var createdResponse = await client.PostAsJsonAsync("/api/userstories", newItem);

            // Assert - An item was created
            Assert.Equal(HttpStatusCode.Created, createdResponse.StatusCode);
            Assert.NotNull(createdResponse.Headers.Location);

            using var createdJson = await createdResponse.Content.ReadFromJsonAsync<JsonDocument>();

            // Arrange - Get the new item's URL and Id
            var itemUri = createdResponse.Headers.Location;

            // Act - Get the item
            var item = await client.GetFromJsonAsync<UserStoryDto>(itemUri);

            // Assert - Verify the item was created correctly
            Assert.NotNull(item);

            if (item != null)
            {
                Assert.Equal("Create a User Story", item.Title);
                Assert.Equal(1, item.Owner);
                Assert.Equal(1, item.Author);
                Assert.Equal(1, item.Champions);
                Assert.Equal(1, item.Teams);
                Assert.Equal(1, item.Customers);
                Assert.Equal(1, item.Products);
                Assert.Equal(1, item.Projects);
                Assert.Equal(DateTime.Parse("2021-12-28"), item.OriginDate);
                Assert.Equal(1, item.State);
                Assert.Equal(1, item.Sequence);
                Assert.Equal("As a <>, I want to <>, so that I can <>.", item.Content);
                Assert.Equal(1, item.UserStoryThemeId);
            }
        }

        [Fact]
        public async Task T3_GetUserStories_Returns_One()
        {
            await using UserStoryApi? application = new();

            var client = application.CreateClient();
            //var response = await client.GetAsync("/api/userstories");

            // Act - Get all the items
            var items = await client.GetFromJsonAsync<List<UserStoryDto>>("/api/userstories");

            // Assert - There should be no items
            if (items != null)
            {
                Assert.Single(items);
            }
        }

        [Fact]
        public async Task T4_PutUserStory()
        {
            UserStoryDto userStoryDto = new()
            {
                Title = "Name Change",
                Content = "Desc Change"
            };

            await using UserStoryApi? application = new();

            var client = application.CreateClient();
            //var response = await client.GetAsync("/api/userstories");

            // Act - Get all the items
            var items = await client.PutAsJsonAsync("/api/userstories/1", userStoryDto);


            var createdJson = await client.GetFromJsonAsync<UserStoryDto>("/api/userstories/1");

            // Assert - Verify the item was created correctly
            Assert.NotNull(createdJson);

            if (createdJson != null)
            {
                Assert.Equal("Name Change", createdJson.Title);
                Assert.Equal("Desc Change", createdJson.Content);
            }
        }

        [Fact]
        public async Task T5_PatchUserStory()
        {
            var patchDoc = new JsonPatchDocument<UserStoryDto>();
            patchDoc.Replace(e => e.Title, "Patch Change");

            await using UserStoryApi? application = new();

            var client = application.CreateClient();
            var serializedDoc = JsonConvert.SerializeObject(patchDoc);
            var requestContent = new StringContent(serializedDoc, Encoding.UTF8, "application/json-patch+json");
            var response = await client.PatchAsync("/api/userstories/1", requestContent);


            var createdJson = await client.GetFromJsonAsync<UserStoryDto>("/api/userstories/1");

            // Assert - Verify the item was created correctly
            Assert.NotNull(createdJson);

            if (createdJson != null)
            {
                Assert.Equal("Patch Change", createdJson.Title);
                Assert.Equal("Desc Change", createdJson.Content);
            }
        }

        [Fact]
        public async Task T6_DeleteUserStory()
        {
            await using UserStoryApi? application = new();

            var client = application.CreateClient();

            // Act - Delete the item
            using var deletedResponse = await client.DeleteAsync("/api/userstories/1");

            // Assert - The item no longer exists
            Assert.Equal(HttpStatusCode.NoContent, deletedResponse.StatusCode);

            var items = await client.GetFromJsonAsync<List<UserStoryDto>>("/api/userstories");

            Assert.NotNull(items);
            Assert.Empty(items);

            // Act
            using var getResponse = await client.GetAsync("/api/userstories/1");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }

        class UserStoryApi : WebApplicationFactory<Program>
        {
            protected override IHost CreateHost(IHostBuilder builder)
            {
                // Add mock/test services to the builder here

                // builder.ConfigureServices(services =>
                // {
                //     services.AddScoped(sp => new SqliteConnection("Data Source=userstories.db"));
                // });

                return base.CreateHost(builder);
            }
        }

        private static class DatabaseOps
        {
            public static void Truncate()
            {
                const string connStr = "Data Source=192.168.1.19; Initial Catalog=UserStoryDB; User Id=sa; Password=sa;";
                var conn = new SqlConnection(connStr);
                conn.Open();

                const string cmdText = "TRUNCATE TABLE UserStories";
                var cmd = new SqlCommand(cmdText, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}